import boto3

INPUT_BUCKET = "harveypawel-uploadbucket"
FILE_NAME = "uploadtest.png"
OBJECT_NAME = "uploadtest.png"

session = boto3.Session(profile_name="ClearDown")
client = session.client("s3")

client.upload_file(FILE_NAME,INPUT_BUCKET,OBJECT_NAME)